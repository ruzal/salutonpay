<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('currency_id')->nullable();
            $table->foreignId('sender_number_id')->nullable();
            $table->foreignId('receiver_number_id')->nullable();
            $table->enum('status',['new','in_progress','successfully_complited'])
                        ->default('new')->nullable();
            $table->boolean('is_archived',false)->nullable();
            $table->foreignId('client_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
