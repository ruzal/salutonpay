<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_units', function (Blueprint $table) {
            $table->id();
            $table->foreignId('get_payment_id')->nullable();
            $table->foreignId('send_payment_id')->nullable();
            $table->enum('status',['new','in_progress','successfully_complited'])
                        ->default('new')->nullable();
            $table->foreignId('employee_id')->nullable();
            $table->boolean('is_archived',false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_units');
    }
}
