@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/dashboard" class="btn btn-outline-primary btn-sm">Панель управления</a>
                <h1 class="display-one">Пользователь: {{ ucfirst($client->name) }}</h1>
                <p>{!! $client->name !!}</p> 
                <hr>
                <form action={{"https://api.telegram.org/bot".config('app.telegram')."/sendMessage"}} method="GET">
                <div class="row">
                    <div class="control-group col-12">
                     <label for="text">написать в телеграм</label>
                     <input type="text" id="text" class="form-control" name="text"
                                           placeholder="текст сообщения" value="" required>
                    <input type="hidden" id="chat_id" name="chat_id" value="{{$client->telegram_id}}">
                    <input type="hidden" id="parse_mode" name="parse_mode" value="html">
                    <button class="mt-2 btn btn-outline-primary">отправить</button>
                    </div>
                </div>
                </form>
                <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                    <h6 class="display-6">Информация</h6>
                    <p>Подробная информация о пользователе</p>

                    <hr>
                        <div class="row">
                            <div class="control-group col-12">
                               <ul>
                               <li>Email: {{$client->email}} </li>
                               <li>Телефон: {{$client->phone}} </li>
                               <li>Член команды SalutonPay: @if ($client->is_employee)
                                    Да
                                    @else
                                    Нет 
                               </li>
                                    @endif
                                   @if ($client->is_admin)
                                   <li>Админ: Да</li>
                                   @else
                                   <li>Админ: Нет </li>
                                   @endif
                               </ul> 
                            </div>
                        </div>
                </div>
                <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                    <h6 class="display-6">Платежи</h6>
                    <p>Подробная информация о платежах</p>

                    <hr>
                        <div class="row">
                        @forelse($client->payments as $payment)
                            <div class="control-group col-12">
                               <ul>
                               <li>Статус: {{$payment->status}} </li>
                               <li><a href="/payments/{{$payment->id}}">платеж #{{$payment->id}}</a></li>
                               </ul> 
                            </div>
                        @empty
                        <div class="control-group col-12">
                        <p>нет платежей</p>
                        </div>
                        @endforelse
                        </div>
                </div>


            </div>
        </div>
    </div>
@endsection
