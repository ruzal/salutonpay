@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
               <div class="row">
                    <div class="col-8">
                        <h1 class="display-one">SalutonPay - прием и отправка платежей по всему миру</h1>
                        <p>	&#128512;</p> <!--😀-->
                    </div>
                    <div class="col-4">
                        <p>Панель управления</p>
                        <a href="/dashboard" class="btn btn-primary btn-sm">Перейти</a>
                    </div>
               </div>
            <p> клиенты и члены команды </p>
            @forelse($clients as $client)
                <ul>
                    <li><a href="/user/{{$client->id}}">{{ucfirst( !(empty($client->name)) ? $client->name : $client->id )}}</a></li>
                </ul>
            @empty
                <p class="text-warning">Нет клиентов</p>
            @endforelse
            </div>
        </div>
    </div>
@endsection
