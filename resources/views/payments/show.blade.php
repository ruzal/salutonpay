@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/dashboard" class="btn btn-outline-primary btn-sm">Панель управления</a>
                <h2 class="display-one">Платеж клиента:
                @if (!empty($payment->client))
                <a href="/user/{{$payment->client->id}}/">{{ ucfirst($payment->client->name) }}</a>
                @endif
                </h2>
                <p>Статус платежа: {{!empty($payment->status) ? $payment->status : "нет"}}</p> 
                <hr>

                    <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                        <h5 class="display-5">Платеж</h5>
                        <p>платеж: #{{$payment->id}}</p>

                        <hr>
                            <div class="row">
                                <div class="control-group col-12">
                                    <ul>
                                        <li>Клиент: 
                               <a href="/user/{{$payment->client->id}}/">{{ !(empty($payment->client->name)) ? $payment->client->name : "имя не указано" }}</a>
                                        </li>
                                        <li>Получение перевода </li> 
                                        <li>счет отправителя: {{!(empty($payment->sender_number->number)) ? $payment->sender_number->number : "не указан" }}</li>
                                        <li>платежная система: {{ !(empty($payment->sender_number->account_type->type)) ? $payment->sender_number->account_type->type : "не указана"}} </li>
                                        <li>счет получателя: {{ !(empty($payment->receiver_number->number)) ? $payment->receiver_number->number : "не указан"}}</li>
                                        <li>Сумма: {{ !(empty($payment->amount)) ? $payment->amount : "не указана"}}</li>
                                        <li>Валюта: {{ !(empty($payment->currency->currency)) ? $payment->currency->currency : "не указана" }} </li>
                                        <li>статус: {{ !(empty($payment->status)) ? $payment->status : "не указан" }}</li>
                                    </ul>
                                </div>
                            </div>
                    
                    </div>

            </div>
        </div>
    </div>
@endsection
