@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
               <div class="row">
                    <div class="col-8">
                        <h1 class="display-one">SalutonPay - прием и отправка платежей по всему миру</h1>
                        <p>Отправляйте платежи за границу и принимайте платежи из-за границы. Для фрилансеров, для оплаты
                           зарубежных сервисов и переводов друзьям. Не требуется банковский аккаунт, простая верификация.</p>
                    </div>
                    <div class="col-4">
                        <p>Панель управления</p>
                        <a href="/dashboard" class="btn btn-primary btn-sm">Перейти</a>
                    </div>
               </div>
            @forelse($payments as $payment)
                <ul>
                    <li><a href="./payments/{{$payment->id}}">#{{$payment->id}} {{ $payment->status}}</a></li>
                </ul>
            @empty
                <p class="text-warning">Нет платежей</p>
            @endforelse
            </div>
        </div>
    </div>
@endsection
