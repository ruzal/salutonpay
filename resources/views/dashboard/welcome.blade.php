@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center pt-5">
                <h1 class="display-one mt-5"> {{ config('app.name')}} </h1>
                <p>Панель управления</p>
                <br>
                <a href="/dashboard" class="btn btn-outline-primary">Перейти</a>
            </div>
        </div>
    </div>
@endsection
