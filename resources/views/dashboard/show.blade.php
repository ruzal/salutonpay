@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/dashboard" class="btn btn-outline-primary btn-sm">Панель управления</a>
                <h2 class="display-one">Обмен проводит:
                @if (!empty($exchangeunit->employee))
                <a href="/user/{{$exchangeunit->employee->id}}/">{{ ucfirst($exchangeunit->employee->name) }}</a>
                @endif
                </h2>
                <p>Статус обмена: {{$exchangeunit->status}}</p> 
                <hr>
                <a href="/exchangeunit/{{$exchangeunit->id}}/edit"  class="btn btn-outline-primary">Изменить статус обмена</a>
                
                <div class="row">
                    <div class="col-6">
                    <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                        <h5 class="display-5">Получить платеж от клиента</h5>
                        <p>платеж: #{{$exchangeunit->get_payment->id}}</p>

                        <hr>
                            <div class="row">
                                <div class="control-group col-12">
                                    <ul>
                                        <li>Клиент-отправитель: 
                               <a href="/user/{{$exchangeunit->get_payment->client->id}}/">{{ $exchangeunit->get_payment->client->name }}</a>
                                        </li>
                                        <li>Получение перевода </li> 
                                        <li>со счета клиента: {{$exchangeunit->get_payment->sender_number->number}}</li>
                                        <li>платежная система: {{$exchangeunit->get_payment->sender_number->account_type->type}} </li>
                                        @if (!empty($exchangeunit->employee))
                                        <li>на счет {{$exchangeunit->employee->name}}-a:
                                                    {{$exchangeunit->get_payment->receiver_number->number }}
                                        </li>
                                        @endif
                                        <li>Сумма: {{$exchangeunit->get_payment->amount}}</li>
                                        <li>Валюта: {{$exchangeunit->get_payment->currency->currency}} </li>
                                        <li>статус: {{$exchangeunit->get_payment->status}}</li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                    </div>
                    <div class="col-6">
                    <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                        <h5 class="display-5">Отправить платеж клиенту</h5>
                        <p>платеж: #{{$exchangeunit->send_payment->id}}</p>

                        <hr>
                            <div class="row">
                            <div class="control-group col-12">
                                    <ul>
                                        <li>Клиент-получатель: 
                               <a href="/user/{{$exchangeunit->send_payment->client->id}}/">{{ $exchangeunit->send_payment->client->name }}</a>
                                        </li>
                                        <li>Отправление перевода </li>
                                        <li> на счет клиента номер: {{$exchangeunit->send_payment->receiver_number->number}}</li>
                                        <li>платежная система: {{$exchangeunit->send_payment->receiver_number->account_type->type}} </li>
                                        @if (!empty($exchangeunit->employee))
                                        <li>со счета {{$exchangeunit->employee->name}}-a:
                                                     {{$exchangeunit->send_payment->sender_number->number }}
                                        </li>
                                        @endif
                                        <li>Сумма: {{$exchangeunit->send_payment->amount}}</li>
                                        <li>Валюта: {{$exchangeunit->send_payment->currency->currency}} </li>
                                        <li>статус: {{$exchangeunit->send_payment->status}}</li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
