@extends('app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/dashboard" class="btn btn-outline-primary btn-sm">Панель управления</a>
                <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                    <h1 class="display-4">Изменить обмен</h1>
                    <p>
                        статусы обмена:<br>
                        new - создан;<br>
                        in_progress - в работе, кто-то из команды начал или собирается начать совершать платежи по обмену;<br>
                        successfully_complited - завершен, каждый платеж по обмену завершен успешно, деньги перечислены, обмен закрыт;   
                    </p>

                    <hr>

                    <form action="" method="POST">
                        @csrf
                        <div class="row">
                            <div class="control-group col-12">
                                <label for="status">Статус:</label>
                                <select  id="status" class="form-control" name="status">
                                    @foreach(\App\Models\ExchangeUnit::statuses as $key=>$value)
                                        @if ($value == $exchangeunit->status)
                                        <option value="{{$key}}" selected>{{$value}}</option>                 
                                        @else
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="employee_id">Кто будет обменивать:</label>
                                <select  id="employee_id" class="form-control" name="employee_id">
                                    @foreach(\App\Models\User::where('is_employee',true)->get() as $user)
                                        @if ((!empty($exchangeunit->employee)) && ($user->id == $exchangeunit->employee->id))
                                        <option value="{{$exchangeunit->employee->id}}" selected>{{$exchangeunit->employee->name}}</option>                 
                                        @else
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <input type="checkbox" id="is_employee" name="is_employee" checked>
                                <label for="is_employee">Я буду проводить обмен</label>
               
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="control-group col-12 text-center">
                                <button id="btn-submit" class="btn btn-primary">
                                    Обновить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
