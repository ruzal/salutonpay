<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;
use Telegram;
use Illuminate\Http\Request;

/**
 * Class HelpCommand.
 */
class MyPaymentsCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'my_payments';

    /**
     * @var array Command Aliases
     */
    protected $aliases = ['mypaymentscommand'];

    /**
     * @var string Command Description
     */
    protected $description = 'My payments';

    /**
     * {@inheritdoc}
     */
    public function handle()
    
    {
        $response = $this->getUpdate();
        
        $text = "Мои переводы";
    
        $this->replyWithMessage(compact('text'));

    }
}

