<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;
use Telegram;
use Illuminate\Http\Request;
use App\Models\TgConversation;
use App\Models\User;

/**
 * Class HelpCommand.
 */
class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    /**
     * @var array Command Aliases
     */
    protected $aliases = ['startcommand'];

    /**
     * @var string Command Description
     */
    protected $description = 'Start command, register user and send available commands';

    /**
     * {@inheritdoc}
     */
    public function handle()
    
    {
        $response = $this->getUpdate();
        
        $telegram_id = $response->getMessage()->chat->id;
        if (is_null($response->getMessage()->from->first_name)) {
        $text = "в телеграмм у вас не установлено имя".chr(10).chr(10);
        } else {
        $text = "в телеграмм у вас установлено имя ". $response->getMessage()->from->first_name ."!".chr(10).chr(10);
        //$text .= " а также вы написали: ".$response->getMessage()->text;
        }
       

       try {
       $user = User::where('telegram_id',$response->getMessage()->from->id)->first();
       } catch (Throwable $e) {
            return false;
       }

       if (is_null($user)) {
            $user = User::create(['telegram_id'=>$response->getMessage()->from->id]);

            $convers = TgConversation::create(['user_id'=>$user->id,
                                            'state'=> 0]);
       }
       else {
            $convers = $user->tgconversation;
       }

       $telegram_text = preg_replace("/\/start/i","",$response->getMessage()->text);
       switch ($convers->state) {
            
                    case 0:
                        if ($telegram_text === '') {
                            $convers->update(['state' => 0]);
                            $this->replyWithMessage([
                                            'chat_id' => $response->getMessage()->chat->id,
                                            'text' => "Как к вам можно обращаться? Напишите: /start Кушим",
                                            ]);
                            break;
                        }
                        
                        $user->update(['name'=>$telegram_text]);
                        $convers->update(['state'=>1]);
                    // no break !
                    case 1:
                       $convers->update(['state'=>0]);// чтобы при след /start снова имя спрашивал, обновление информации
                       $text .= "Теперь вам доступны команды";    
                       $button1 = Telegram\Bot\Keyboard\Keyboard::button(['text'=>'/get_payment']);
                       $button2 = Telegram\Bot\Keyboard\Keyboard::button(['text'=>'/send_payment']);
                       //$button3 = Telegram\Bot\Keyboard\Keyboard::button(['text'=>'/my_payments']);         
          
                       $keyboard = new Telegram\Bot\Keyboard\Keyboard(['resize_keyboard'=>true,'one_time_keyboard'=>true]);
                       $keyboard->row($button1,$button2);

                       $new_response = Telegram::sendMessage([
                                            'chat_id' => $response->getMessage()->chat->id,
                                            'text' => $telegram_text.", можете оставлять заявки на получение и отправку платежей",
                                            'reply_markup' => $keyboard,
                                            ]);
               }


       /*
       $button1 = Telegram\Bot\Keyboard\Keyboard::inlineButton(['text'=>'/get_payment']);
       $button2 = Telegram\Bot\Keyboard\Keyboard::inlineBbutton(['text'=>'/send_payment']);
       $button3 = Telegram\Bot\Keyboard\Keyboard::inlineBbutton(['text'=>'/my_payments']);*/

       

        //$keyboard = new Telegram\Bot\Keyboard\Keyboard(['resize_keyboard'=>true,'one_time_keyboard'=>true]);
        //$keyboard->inline();       


        /*
        $new_response = Telegram::sendMessage([
                            'chat_id' => $response->getMessage()->chat->id,
                            'text' => 'Отправляйте и получайте переводы по всему миру',
                            'reply_markup' => $keyboard,
                            ]);
        */

        
        
        $this->replyWithMessage(compact('text'));

    }
}

