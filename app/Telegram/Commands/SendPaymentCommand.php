<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;
use Telegram;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Payment;

/**
 * Class HelpCommand.
 */
class SendPaymentCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'send_payment';

    /**
     * @var array Command Aliases
     */
    protected $aliases = ['sendpaymentcommand'];

    /**
     * @var string Command Description
     */
    protected $description = 'Send payment';

    /**
     * {@inheritdoc}
     */
    public function handle()
    
    {
        $response = $this->getUpdate();
        
       try {
       $user = User::where('telegram_id',$response->getMessage()->from->id)->first();
       } catch (Throwable $e) {
            return false;
       }

       if (is_null($user)) {
          $text = "Нажмите на /start , чтобы заполнить имя ";
          $this->replyWithMessage(compact('text'));
          return;
       }
       else {
            $convers = $user->tgconversation;
       }

       $telegram_text = preg_replace("/\/send_payment/i","",$response->getMessage()->text);

       if ($telegram_text === '') {
                        $text = "Напишите номер счета (карты/кошелька), на который хотите отправить перевод, а также валюту и количество денег";
                        $this->replyWithMessage(compact('text'));
   
                        Telegram::sendMessage([
                                            'chat_id' => $response->getMessage()->chat->id,
                                            'text' => "например: /send_payment 10 000 рублей mastercard 5110 0001 3456 7579",
                                            ]);
       } else {
                        $payment = Payment::create(['client_id'=>$user->id]);

                        Telegram::sendMessage([
                                            'chat_id' => $response->getMessage()->chat->id,
                                            'text' => "#".$payment->id." заявка сохранена: ".$telegram_text,
                                            ]);          

        }

    }
}

