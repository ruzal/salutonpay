<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;
use Telegram;

/**
 * Class HelpCommand.
 */
class HelpCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'help';

    /**
     * @var array Command Aliases
     */
    protected $aliases = ['listcommands'];

    /**
     * @var string Command Description
     */
    protected $description = 'Help command, Get a list of all commands';

    /**
     * {@inheritdoc}
     */
    public function handle()
    
    {
        $response = $this->getUpdate();

        /*
        $text = 'Получить платеж /get_payment'.chr(10).chr(10);
        $text .= 'Отправить платеж /send_payment'.chr(10).chr(10);
        $text .= 'Мои платежи /my_payments';
        */
        $text = "нажмите сюда: /start";
        
        $this->replyWithMessage(compact('text'));

    }
}

