<?php

namespace App\Http\Controllers;

use App\Models\ExchangeUnit;
use Illuminate\Http\Request;

class ExchangeUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExchangeUnit  $exchangeUnit
     * @return \Illuminate\Http\Response
     */
    public function show(ExchangeUnit $exchangeUnit)
    {
        return view('dashboard.show',[
                    'exchangeunit' => $exchangeUnit,
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExchangeUnit  $exchangeUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(ExchangeUnit $exchangeUnit)
    {
        return view('dashboard.edit', [
                    'exchangeunit' => $exchangeUnit,                            
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExchangeUnit  $exchangeUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExchangeUnit $exchangeUnit)
    {
        $exchangeUnit->update([
            'status' => $request->status,
            'employee_id' => empty($request->is_employee) ? null : $request->employee_id,
        ]);        

        return redirect("exchangeunit/" . $exchangeUnit->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExchangeUnit  $exchangeUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExchangeUnit $exchangeUnit)
    {
        //
    }
}
