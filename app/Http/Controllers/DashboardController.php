<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExchangeUnit as ExchangeUnit;

class DashboardController extends Controller
{
    public function index()
    {
        $exchangeunits = \App\Models\ExchangeUnit::all();
        return view('dashboard.index', [
                'exchangeunits' => $exchangeunits,
        ]);
    }
}
