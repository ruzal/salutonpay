<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use User;

class TgConversation extends Model
{
    use HasFactory;

    protected $table = 'tgconversations';

    protected $fillable = [
        'user_id',
        'state',
    ];

    public function user()
    {

        return $this->belongsTo('\App\Models\User','user_id','id');

    }
}
