<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Payments;

class Currency extends Model
{
    use HasFactory;

    protected $table = 'currencies';

    protected $fillable = [
        'currency',
        'shortcode',
    ];
    public function payments() 
    {
        return $this->hasMany('App\Models\Payments','id','currency_id');
    }
}
