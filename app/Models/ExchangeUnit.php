<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Payment;
use User;

class ExchangeUnit extends Model
{
    use HasFactory;

    public const statuses =
    [ 
      'new' => 'new',
      'in_progress' => 'in_progress',
      'successfully_complited' => 'successfully_complited',
    ];

    protected $fillable = [
        'status',
        'employee_id',
    ];
    
    protected $table = 'exchange_units';

    public function get_payment()
    {
        
        return $this->belongsTo('App\Models\Payment','get_payment_id','id');

    }

    public function send_payment()
    {
        
        return $this->belongsTo('App\Models\Payment','send_payment_id','id');

    }

    public function employee()
    {

        return $this->belongsTo('App\Models\User','employee_id','id');

    }
}
