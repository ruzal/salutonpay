<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use AccountType;

class AccountNumber extends Model
{
    use HasFactory;

    protected $table = 'account_numbers';

    public function account_type() 
    {
        return $this->belongsTo('App\Models\AccountType','account_type_id','id');
    }
}
