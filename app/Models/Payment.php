<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Currency;
use AccountNumber;
use Client;

class Payment extends Model
{
    use HasFactory;
    
    protected $table = 'payments';

    protected $fillable = [
        'amount',
        'client_id',
    ];

    public function currency()
    {

       return $this->belongsTo('App\Models\Currency','currency_id','id');         
        
    }

    public function sender_number()
    {

       return $this->belongsTo('App\Models\AccountNumber','sender_number_id','id');         
        
    }

    public function receiver_number()
    {

       return $this->belongsTo('App\Models\AccountNumber','receiver_number_id','id');         
        
    }

    public function client()
    {

       return $this->belongsTo('App\Models\User','client_id','id');         
        
    }

}
