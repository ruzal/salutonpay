<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use User;
use AccountNumber;

class UserAccountNumber extends Model
{
    use HasFactory;

    protected $table = 'users_account_numbers';

    public function user()
    {
        
        $this->belongsTo('\App\Models\User','user_id','id');

    }

    public function accountNumber()
    {

        $this->belongsTo('\App\Models\AccountNumber','account_number_id','id');
        
    }
}
