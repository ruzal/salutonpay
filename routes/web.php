<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardControllel;
use App\Http\Controllers\UserControllel;
use App\Http\Controllers\ExchangeUnitController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PaymentController;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard.welcome');
});
Route::get('/dashboard',[\App\Http\Controllers\DashboardController::class, 'index']);
Route::get('/clients', [\App\Http\Controllers\UserController::class,'index']);
Route::get("/user/{user}", [\App\Http\Controllers\UserController::class, 'show']);
Route::get("/exchangeunit/{exchangeUnit}", [\App\Http\Controllers\ExchangeUnitController::class, 'show']);
Route::get("/exchangeunit/{exchangeUnit}/edit", [\App\Http\Controllers\ExchangeUnitController::class, 'edit']);
Route::post("/exchangeunit/{exchangeUnit}/edit", [\App\Http\Controllers\ExchangeUnitController::class, 'update']);
Route::get("/payments", [\App\Http\Controllers\PaymentController::class, 'index']);
Route::get("/payments/{payment}",[\App\Http\Controllers\PaymentController::class,'show']);
Route::post("/taouranqycfzyasctdswievikwyhsgqgmlglyuglukxnemoppv/webhook", function(Request $request) {$update = Telegram::commandsHandler(true); });
Route::get('/setwebhook', function () {
	$response = Telegram::setWebhook(['url' => 'https://salutonpay.onrender.com/taouranqycfzyasctdswievikwyhsgqgmlglyuglukxnemoppv/webhook']);
	dd($response);
});
/*
Route::get('/blog', [\App\Http\Controllers\BlogPostController::class, 'index']);
Route::get("/blog/{blogPost}", [\App\Http\Controllers\BlogPostController::class, 'show']);
Route::get('/blog/create/post',[\App\Http\Controllers\BlogPostController::class, 'create']);
Route::post('/blog/create/post', [\App\Http\Controllers\BlogPostController::class, 'store']);
Route::get("/blog/{blogPost}/edit", [\App\Http\Controllers\BlogPostController::class,'edit']);
Route::put("/blog/{blogPost}/edit", [\App\Http\Controllers\BlogPostController::class,'update']);
Route::delete("/blog/{blogPost}", [\App\Http\Controllers\BlogPostController::class,'destroy']);*/
